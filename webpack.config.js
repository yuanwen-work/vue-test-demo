//1.  该文件的名称默认情况下，只能使用webpack.config.js 后期可以改
//2. 该文件需要使用 commonjs 规范导出配置项(配置项该如何写？)
const path = require('path'); // nodejs 自带的核心模块，负责路径的解析 resovle join

// 这个插件会为你生成一个 HTML5文件，使用脚本标签在文件体中包含你所有的 webpack 包。只需将插件添加到你的 webpack 配置中
const HtmlWebpackPlugin = require('html-webpack-plugin'); //引入插件
module.exports={
    mode: 'development',
    entry:{
        path:'./src/main.js'
    },
    output:{
        path: path.join(__dirname,'dist'),
        filename: 'bundle.js',
    },
    // devtool: 'inline-source-map',
    module: {
      rules: [{
        test: /\.less$/,
        use: [{
            loader: "style-loader" // creates style nodes from JS strings
        }, {
            loader: "css-loader" // translates CSS into CommonJS
        }, {
            loader: "less-loader" // compiles Less to CSS
        }]
    }]
    },
    plugins: [                    //配置插件
        new HtmlWebpackPlugin({
            filename: 'index.html', //生成后的文件名
            template: 'src/templates/index.html'  //文件所在位置
        })
    ]
}