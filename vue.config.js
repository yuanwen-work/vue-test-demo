module.exports = {
    //Solution For Issue:You are using the runtime-only build of Vue where the template compiler is not available. Either pre-compile the templates into render functions, or use the compiler-included build.
    //zhengkai.blog.csdn.net
    runtimeCompiler: true,
    devServer: {
      open: true,
      host: 'localhost',
      port: 8080,
      proxy: {
          '/api': {
              target: 'http://hn216.api.yesapi.cn',
              changeOrigin: true,
              pathRewrite: {
                  '^/api': '/'
              }
          }
      }
  },
  }