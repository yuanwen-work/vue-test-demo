import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);
const state={
    count:1
}
const mutations={
    add(state,n){
        state.count+=n;
    },
    reduce(state){
        state.count--;
    }
}
const getters = {
    other:function(state){
        state.count +=100;
        console.log('state.count的新值为:',state.count)
        return state.count;
    }
}
const actions ={
    addAction(context){
        console.log('context',context);
        context.commit('add',10)
        setTimeout(()=>{context.commit('add',10)},3000);
        console.log('我比reduce提前执行');
    },
    reduceAction({commit}){
        setTimeout(()=>{commit('reduce')},3000);
    }
}
export default new Vuex.Store({
    state,mutations,getters,actions
})