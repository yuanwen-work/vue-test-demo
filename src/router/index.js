import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Count from '../components/count.vue'
import Error from '../views/error.vue'
import vuexTest from '../components/vuexTest.vue'
Vue.use(VueRouter)

const routes = [{
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/count',
    name: 'Count',
    component: Count,
    // beforeEnter: (to, from, next) => {
    //   router.push('/')
    // }
  },
  {
    path: '/vuexTest',
    name: 'vuexTest',
    component: vuexTest
  },
  {
    path: '/routeFather',
    name: 'routeFather',
    component: () => import( /* webpackChunkName: "echarts" */ '../components/routeFather.vue'),
    children: [{
        path: '/',
        name: 'routeFather',
        component: () => import( /* webpackChunkName: "echarts" */ '../components/routeFather.vue'),
      },
      {
        path: '/routeFather/routeSon1',
        name: 'routeSon1',
        component: () => import( /* webpackChunkName: "echarts" */ '../components/routeSon1.vue'),
      },
      {
        path: '/routeFather/routeSon2',
        name: 'routeSon2',
        component: () => import( /* webpackChunkName: "echarts" */ '../components/routeSon2.vue'),
      }
    ]
  },
  // 通过url传递参数
  {
    path: '/urlAndParams/:newId(\\d+)/:newTitle',
    name: 'urlAndParams',
    component: () => import( /* webpackChunkName: "about" */ '../components/urlAndParams.vue')
  },
  {
    path: '/goback',
    name: 'goback',
    redirect: '/'
  },
  {
    path: '*',
    component: Error
  },
  {
    path: '/about',
    name: 'About',
    component: () => import( /* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/directives',
    name: 'test',
    component: () => import( /* webpackChunkName: "directives-test" */ '../views/directives-test.vue')
  },
  {
    path: '/tongxin-father',
    name: 'tongxin-father',
    component: () => import( /* webpackChunkName: "tongxin-father" */ '../views/tongxin-father.vue')
  },
  // {
  //   path: '/tongxin-son',
  //   name: 'tongxin-son',
  //   component: () => import(/* webpackChunkName: "tongxin-son" */ '../views/tongxin-son.vue')
  // },
  {
    path: '/pagination',
    name: 'pagination',
    component: () => import( /* webpackChunkName: "Pagination" */ '../views/Pagination.vue'),
    meta: {
      keepAlive: true // 需要缓存
    }
  }, {
    path: '/international',
    name: 'international',
    component: () => import( /* webpackChunkName: "international" */ '../views/international.vue')
  }, {
    path: '/mock',
    name: 'mock',
    component: () => import( /* webpackChunkName: "mock" */ '../components/mock.vue'),
    // meta: {
    //   keepAlive: true  // 需要缓存
    //   }
  }, {
    path: '/echarts',
    name: 'echarts',
    component: () => import( /* webpackChunkName: "echarts" */ '../components/Echarts-demo.vue'),
  }, {
    path: '/echarts-map',
    name: 'echarts-map',
    component: () => import( /* webpackChunkName: "echarts" */ '../components/Echarts-map.vue')
  }, {
    path: '/three-Level-Component',
    name: 'three-Level-Component',
    component: () => import( /* webpackChunkName: "echarts" */ '../components/threeLevelComponent.vue')
  }, {
    path: '/checkAndErwei',
    name: 'checkAndErwei',
    component: () => import( /* webpackChunkName: "echarts" */ '../views/checkAndErwei.vue')
  }, {
    path: '/eventBusFather',
    name: 'eventBusFather',
    component: () => import( /* webpackChunkName: "echarts" */ '../components/eventBusFather.vue'),
  },{
    path: '/eventBusSon1',
      name: 'eventBusSon1',
      component: () => import( /* webpackChunkName: "echarts" */ '../components/eventBusSon1.vue'),
  },{
    path: '/eventBusSon2',
    name: 'eventBusSon2',
    component: () => import( /* webpackChunkName: "echarts" */ '../components/eventBusSon2.vue'),
  },{
    path: '/echartLine',
    name: 'echartLine',
    component: () => import( /* webpackChunkName: "echarts" */ '../components/Echart-line.vue'),
  }

]
// 防止xss攻击
var xss = require("xss");
var html = xss('<script>alert("xss");</script>');
alert(html);

const router = new VueRouter({
  mode: 'history',
  routes,
  //   scrollBehavior (to, from, savedPosition) {
  //     console.log('滚动行为的值:',savedPosition)
  //     if (savedPosition) {
  //         return savedPosition
  //     }   else {
  //         if (from.meta.keepAlive) {
  //             from.meta.savedPosition = document.body.scrollTop;
  //         }
  //         return { x: 0, y: to.meta.savedPosition || 0 }
  //     }
  // }
})

// router.beforeEach((to, from, next) => {
//   console.log('to',to);
//   console.log('from',from);
//   if (to.path === '/count') {
//     next({path: '/about'})
//   } else {
//     next();
//   }
// })

// router.afterEach((to,from)=>{
//   console.log('afterTo',to);
//   console.log('afterFrom',from);
// })
export default router