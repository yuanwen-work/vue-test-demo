const myMixin = {
    data() {
        return {
            isNoData: false,
            isShowLoding: true
        }
    }
}
export default myMixin;