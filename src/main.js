import Vue from 'vue'
import App from './App.vue'
import router from './router'
//element-ui组件
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
//国际化
import vi18n from 'vue-i18n';
//全局引入数据可视化
// import * as echarts from 'echarts';
// Vue.prototype.$echarts = echarts 
require('./mock.js');
// Vue.config.productionTip = false  以阻止 vue 在启动时生成生产提示
Vue.config.productionTip = false  
//全局使用国际化组件
Vue.use(vi18n);
const i18n = new vi18n({
  locale: 'zh',
  messages: {
    "zh": require('@/lang/zh-CN'),
    "en": require('@/lang/en-US'),
  },
});
Vue.use(ElementUI, {
  i18n: (key, value) => i18n.t(key, value)
});
new Vue({
  router,
  i18n,
  // echarts,
  render: h => h(App),
  data: {
    eventHub: new Vue(),
  }
}).$mount('#app')
